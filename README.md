Hypercube 300 Cable Chain
=========================

This started as two chains connected from the X carriage through an XY
joiner to a frame mount, but then I realized that by rotating the chain 90°
so that it flexes in the same plane as the CoreXY mechanism, the entire area
can be covered by one chain.  It should allow for shorter cables, too
(though I'll probably still have to extend the hotend wiring).

The chain uses the same links I've used with cable chains on an Anet A8 for
the better part of a year, cribbed from [https://www.thingiverse.com/thing:2115095](https://www.thingiverse.com/thing:2115095).

The frame mount, carriage mount, and an as-yet-undetermined number of chain
links are needed for any installation.  If you're using my [parametric extrusion brackets](https://www.thingiverse.com/thing:3012597)
to hold your frame together, you might also need the chain guide to keep it
from snagging on the front left corner.

