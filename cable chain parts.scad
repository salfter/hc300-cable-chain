$fn=30; // increase to 90 for production

// development use
fit_check();

// pieces to print (one each; render one at a time):
// carriage_section()
// frame_section()

// translate([-5,20,4])
// rotate([90,0,0])
//     carriage_section();

// translate([-65,-120,10])
// rotate([-90,0,0])
//     frame_section();

// fit check
module fit_check()
{
    hc300_mockup();
    translate([113,9,43.25])
        carriage_section();
    translate([-38.5,180,-42])
        frame_section();
}

// mockups: XY joiner, carriage, rods, and part of the frame
module hc300_mockup()
{
    translate([-27,0,0])
    rotate([0,90,0])
        %import("../spiral bearing XY joiner/spiral bushing XY joiner.stl");

    translate([60,0,25.8])
    rotate([0,90,0])
        %difference()
        {
            cylinder(d=10, h=200, center=true);
            cylinder(d=8, h=201, center=true);
        }
    translate([60,0,-25.8])
    rotate([0,90,0])
        %difference()
        {
            cylinder(d=10, h=200, center=true);
            cylinder(d=8, h=201, center=true);
        }

    translate([115,-18.5,53.5])
    rotate([90,0,90])
        %import("../piezo_carriage/Hypercube_300_Orion_Piezo_Carriage_Igus.stl");

    translate([-27,70,0])
    rotate([90,0,0])
        %cylinder(d=8, h=200, center=true);

    translate([-2.25,170,0])
    rotate([0,90,-90])
        %import("../original/HC_Y_Shaft_Clamp_8mm_Make_3.stl"); 

    translate([-27,180,-100])
        %cube([20,20,300], center=true);

    translate([-27,180,46.9])
    rotate([-90,0,-90])
        %import("../../frame_braces/hc300_top_corner.stl");

    translate([-27,70,46.9])
        %cube([20,200,20], center=true);

    translate([-10,144,-17])
    rotate([180,0,-90])
        %import("../original/HC300_XY_Motor_right_Make_1.stl");

    translate([-27,180,-91])
    rotate([0,0,90])
    mirror([1,0,0])
        %import("../../frame_braces/hc300_front_left_t.stl");

    translate([-4,142,-63])
        %import("../../z_carriage/single_z_motor/tmp/Nema 17HS4401-S.stl");

    translate([120,180,46.9])
    rotate([0,90,0])
        %cube([20,20,300], center=true);
}

// carriage section

module carriage_section()
{
    carriage_mountpoint();
    translate([10,0,0])
        carriage_mountpoint();

    translate([5,0,20.5])
    cube([10,8,18], center=true);

    translate([-2, -4, 28+12])
    rotate([-90,0,0])
        chain_f();

}

module carriage_mountpoint()
{
    difference()
    {
        minkowski()
        {
            union()
            {
                translate([0,0,6+6])
                    cube([3, 7, 24.5+12], center=true);
                translate([0,0,-6.25])
                rotate([0,90,0])
                    cylinder(d=7, h=3, center=true);
            }
            sphere(d=1);
        }
        translate([0,0,6.25])
        rotate([0,90,0])
            cylinder(d=3.4, h=5, center=true);
        translate([0,0,-6.25])
        rotate([0,90,0])
            cylinder(d=3.4, h=5, center=true);
    }
}

// frame section

module frame_section()
{
    translate([61,0,100.5])
    difference()
    {
        union()
        {
            cube([20,20,3], center=true);

            translate([4,8.5,10])
                cube([12,3,20], center=true);

            translate([7,-4,-1.5])
                cube([3,14,17]);

            translate([10,10,25])
            rotate([-90,0,180])
                chain_m();
        }
        translate([0,0,23])
        {
            cylinder(d=5.4, h=50, center=true);
            translate([0,0,3])
                cube([5.4,7,46], center=true);
        }
    }
}

// split a chain link into male & female halves for integration with the mounts

module chain_f()
{
    intersection()
    {
        translate([-10,13,0])
            import("Anet_A8_Chain.stl");
        translate([0,-15,0])
        cube([20,30,15]);
    }
}

module chain_m()
{
    difference()
    {
        union()
        {
            intersection()
            {
                translate([-10,13,0])
                    import("Anet_A8_Chain.stl");
                translate([-11,-15,0])
                cube([20,30,15]);
            }
            translate([9,0,1.2])
                cube([6,22.5,2.4], center=true);
            translate([9,-10,6.7])
                cube([6,2.5,13.4], center=true);
            translate([9,10,6.7])
                cube([6,2.5,13.4], center=true);
        }
        translate([5.5,-8.75,2.4])
            cube([6.5,17.5,11]);
    }
}
